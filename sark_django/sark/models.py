import datetime

from django.db import models


class TimestampedModel(models.Model):
    date_added = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class DateApproximationLevel(models.Model):
    approximation = models.CharField(max_length=50)

    def __str__(self):
        return self.approximation


class Role(models.Model):
    role = models.CharField(max_length=200)

    def __str__(self):
        return self.role

    class Meta:
        ordering = ('role',)


class Genre(models.Model):
    genre = models.CharField(max_length=50)
    description = models.TextField(blank=True, default="")

    def __str__(self):
        return self.genre

    class Meta:
        ordering = ('genre',)


class CopyrightStatus(models.Model):
    status = models.CharField(max_length=50)

    def __str__(self):
        return self.status

    class Meta:
        verbose_name_plural = "copyright statuses"


class InstrumentFamily(models.Model):
    family = models.CharField(max_length=100)

    def __str__(self):
        return self.family

    class Meta:
        verbose_name_plural = "instrument families"


class Instrument(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True, default="")
    family = models.ForeignKey(InstrumentFamily, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('name',)


class Location(TimestampedModel):
    city = models.CharField(max_length=100, blank=True)
    country = models.CharField(max_length=100)
    zoom = models.IntegerField(default=10)
    normalized_location = models.ForeignKey("self", on_delete=models.SET_NULL, blank=True, null=True)
    normalization_notes = models.CharField(max_length=150, blank=True, null=True)

    def __str__(self):
        if self.city:
            return "{0}, {1}".format(self.city, self.country)
        else:
            return self.country

    class Meta:
        ordering = ('country', 'city')


class Image(TimestampedModel):
    file = models.ImageField(upload_to="img", height_field='image_height', width_field='image_width')
    thumb = models.ImageField(upload_to="img")
    image_height = models.PositiveIntegerField(null=True, editable=False)
    image_width = models.PositiveIntegerField(null=True, editable=False)
    description = models.CharField(blank=True, null=True, max_length=300)
    date = models.DateField(blank=True, null=True)
    sort_order = models.IntegerField(default=2)

    def __str__(self):
        return self.file.name

    class Meta:
        ordering = ('date', 'sort_order',)


class Agent(TimestampedModel):
    name = models.CharField(max_length=200)
    primary_place_of_activity = models.ForeignKey(Location, null=True, blank=True, on_delete=models.CASCADE)
    birthdate = models.DateField(blank=True, null=True)
    deathdate = models.DateField(blank=True, null=True)
    dates_active = models.CharField(blank=True, null=True, max_length=20)
    bio = models.CharField(default="No bio on record", max_length=2000)
    primary_role = models.ForeignKey(Role, blank=True, null=True, on_delete=models.CASCADE)
    members = models.ManyToManyField("self", blank=True)
    photos = models.ManyToManyField(Image, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('name',)


class Performance(TimestampedModel):
    title = models.CharField(max_length=200)
    date = models.DateField(blank=True, null=True)
    date_text = models.CharField(max_length=25, blank=True, null=True)
    date_accuracy = models.ForeignKey(DateApproximationLevel, blank=True, null=True, on_delete=models.CASCADE)

    audio = models.FileField(upload_to="audio", null=True)
    original_locations = models.ManyToManyField(Location, blank=True)
    normalized_locations = models.ManyToManyField(Location, blank=True, related_name="performances_in_location")
    instruments = models.ManyToManyField(Instrument, blank=True)
    genres = models.ManyToManyField(Genre, blank=True)
    associated_agents = models.ManyToManyField(Agent, through='AssociatedAgent', blank=True)
    has_had_locations_vetted = models.BooleanField(blank=False, default=False)

    photos = models.ManyToManyField(Image, blank=True)

    def __str__(self):
        return "{0} - {1}".format(self.title, str(self.date))

    class Meta:
        ordering = ('date', 'title')


class Reel(TimestampedModel):
    title = models.CharField(max_length=300)
    barcode = models.CharField(max_length=300)
    description = models.CharField(max_length=200, blank=True)
    date_text = models.CharField(max_length=25, blank=True, null=True)
    date_accuracy = models.ForeignKey(DateApproximationLevel, blank=True, null=True, on_delete=models.CASCADE)
    date = models.DateField(blank=True, null=True)
    performances = models.ManyToManyField(Performance, blank=True)
    images = models.ManyToManyField(Image, blank=True)
    is_copyright_protected = models.BooleanField(blank=False, default=True)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if self.date_text:
            normalized_date = self.date_text + "-01-01"
            normalized_date = normalized_date[:10]
            year, month, day = normalized_date.split("-")

            self.date = datetime.date(int(year), int(month), int(day))

        super(Reel, self).save(*args, **kwargs)

    class Meta:
        abstract = True


class RadioShow(Reel):
    host = models.ForeignKey(Agent, on_delete=models.CASCADE)
    script_text = models.TextField(blank=True)
    script_images = models.ManyToManyField(Image, blank=True, related_name="script_images")
    completeness = models.CharField(blank=False, default="Platinum", max_length=25)

    class Meta:
        ordering = ('date',)


class FieldRecording(Reel):
    unique_id = models.BigIntegerField()
    recording_engineer = models.ForeignKey(Agent, blank=True, null=True, on_delete=models.CASCADE)
    location = models.ForeignKey(Location, blank=True, null=True, on_delete=models.CASCADE)


class AssociatedAgent(models.Model):
    agent = models.ForeignKey(Agent, on_delete=models.CASCADE)
    recording = models.ForeignKey(Performance, on_delete=models.CASCADE)
    roles = models.ManyToManyField(Role, blank=True)

    def __str__(self):
        return "{} -- {}".format(self.agent, self.recording)

    class Meta:
        verbose_name = 'Roles played by agent in broadcast'
        verbose_name_plural = 'Roles played by agent in broadcast'
