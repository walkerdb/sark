from __future__ import absolute_import
from django.urls import resolve
from django.test import TestCase, Client
from django.urls import reverse

from .views import home
from .models import RadioShow


class UrlResolutionTest(TestCase):
    def setUp(self):
        pass
        self.client = Client()

    def setUpTestData(cls):
        r = RadioShow.objects.create()
        r.save()
        pass

    def test_root_url_resolves_to_home_page_view(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def test_home_page_renders(self):
        response = self.client.get(reverse("demo"))
        templates_used = [t.name for t in response.templates]

        self.assertEqual(response.status_code, 200)
        self.assertIn("home.html", templates_used)

    def test_about_page_renders(self):
        response = self.client.get(reverse("about"))
        templates_used = [t.name for t in response.templates]

        self.assertEqual(response.status_code, 200)
        self.assertIn("about.html", templates_used)

    def test_broadcast_page_renders(self):
        response = self.client.get(reverse("broadcast", args=["2000", "01", "01"]))
        templates_used = [t.name for t in response.templates]

        self.assertEqual(response.status_code, 200)
        self.assertIn("broadcast.html", templates_used)