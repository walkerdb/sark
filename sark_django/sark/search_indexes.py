from django.shortcuts import get_object_or_404
from haystack import indexes

from sark_django.sark.models import Role, AssociatedAgent
from . import models


class ProgramIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    date = indexes.DateField(model_attr="date")
    host = indexes.CharField(model_attr="host", faceted=True)
    faceted_model_type = indexes.CharField(faceted=True)
    completeness = indexes.CharField(faceted=True, model_attr="completeness")
    is_copyright_protected = indexes.BooleanField(faceted=True, model_attr="is_copyright_protected")

    performers = indexes.MultiValueField(faceted=True)
    locations = indexes.MultiValueField(faceted=True)

    def get_model(self):
        return models.RadioShow

    @staticmethod
    def prepare_performers(obj):
        performer_role = get_object_or_404(Role, role="Performer")
        performers = []
        for performance in obj.performances.all():
            associated_agents = AssociatedAgent.objects.filter(recording=performance)
            performers.extend([aa.agent for aa in associated_agents if performer_role in aa.roles.all()])
        return performers

    @staticmethod
    def prepare_locations(obj):
        locations = []
        for performance in obj.performances.all():
            locations.extend([location.country for location in performance.original_locations.all()])
            locations.extend([location.country for location in performance.normalized_locations.all()])
        return list(set(locations))

    @staticmethod
    def prepare_faceted_model_type(obj):
        return 'Radio broadcast'

    def index_queryset(self, using=None):
        return self.get_model().objects


class FieldRecordingIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    date = indexes.DateField(model_attr="date", null=True)
    location = indexes.CharField(model_attr="location", faceted=True, null=True)
    performers = indexes.MultiValueField(faceted=True)
    faceted_model_type = indexes.CharField(faceted=True)

    def get_model(self):
        return models.FieldRecording

    @staticmethod
    def prepare_performers(obj):
        performances = [performance for performance in obj.performances.all()]
        performers = []
        for performance in performances:
            for performer in performance.associated_agents.all():
                performers.append(performer.agent)
        return performers

    # def prepare_location(self, obj):
    #     return '' if not obj.location else obj.location

    @staticmethod
    def prepare_faceted_model_type(obj):
        return 'Field recording'

    def index_queryset(self, using=None):
        return self.get_model().objects.all()


class AgentIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, model_attr="name", faceted=True)
    faceted_model_type = indexes.CharField(faceted=True)

    def get_model(self):
        return models.Agent

    @staticmethod
    def prepare_faceted_model_type(obj):
        return 'People and groups'

    def index_queryset(self, using=None):
        return self.get_model().objects
