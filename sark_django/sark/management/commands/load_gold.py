from django.core.management.base import BaseCommand, CommandError
import traceback

from sark_django.migration_scripts.import_xml_to_db import import_gold_recordings


class Command(BaseCommand):
    def handle(self, *args, **options):
        self.handle_noargs()

    def handle_noargs(self, **options):
        self.stdout.write("importing data from gold recordings...")
        try:
            import_gold_recordings()
        except Exception as e:
            traceback.print_exc()
            raise CommandError("error loading gold data: ", e)

        self.stdout.write(self.style.SUCCESS("Data import successful"))
