from django.core.management.base import BaseCommand

from ... import models as m


class Command(BaseCommand):
    help = 'python manage.py remove_copyright_from_recordings path/to/csv/with/broadcast/barcodes'

    def handle(self, *args, **options):
        reels = m.RadioShow.objects.all()
        for reel in reels:
            reel.is_copyright_protected = False
            reel.save()

        self.stdout.write(self.style.SUCCESS('Removed copyright status for all broadcasts'))
