import os
from django.core.management.base import BaseCommand, CommandError
from ... import models as m


class Command(BaseCommand):
    help = 'python manage.py remove_copyright_from_recordings path/to/csv/with/broadcast/barcodes'

    def add_arguments(self, parser):
        parser.add_argument("filepath")

    def handle(self, *args, **options):
        path_to_barcodes_file = options['filepath']
        if not os.path.isfile(path_to_barcodes_file):
            raise CommandError("provided barcode file {} does not exist".format(path_to_barcodes_file))

        with open(path_to_barcodes_file) as f:
            barcodes = [str(b).strip(" \n") for b in f.readlines()]

        for barcode in barcodes:
            try:
                reel = m.RadioShow.objects.get(barcode=barcode)
            except Exception as e:
                print('Broadcast with barcode {} failed to update: {}'.format(barcode, e))
                continue

            reel.is_copyright_protected = False
            reel.save()

        self.stdout.write(self.style.SUCCESS('Removed copyright status for all provided broadcasts'))
