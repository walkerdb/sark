from django.core.management.base import BaseCommand, CommandError
import traceback

from sark_django.migration_scripts.import_xml_to_db import import_mods_data


class Command(BaseCommand):
    def handle(self, *args, **options):
        self.handle_noargs()

    def handle_noargs(self, **options):
        self.stdout.write("importing data from rescarta sources...")
        try:
            import_mods_data()
        except Exception as e:
            traceback.print_exc()
            raise CommandError("error loading rescarta data: ", e)

        self.stdout.write(self.style.SUCCESS("Data import successful"))
