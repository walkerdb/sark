import random

from django import template
from django.template import engines
from django.template.context import Context
from django.template.defaultfilters import stringfilter

register = template.Library()


@register.filter
@stringfilter
def render_template(value):
    return engines['django'].from_string(value).render(Context().flatten())


@register.filter
@stringfilter
def completeness(value):
    completeness_map = {
        "Platinum": "Broadcast + script",
        "Gold": "Broadcast only",
        "Silver": "Music only + script",
        "Bronze": "Script only"
    }

    default_value = completeness_map["Bronze"]
    return completeness_map.get(value, default_value)


@register.filter
def get_show_tile_image_from_country(show):
    random.seed(show.id)

    country = ""
    performance = show.performances.first()
    countries = []
    if performance:
        countries = [location.country.lower().replace(" ", "-") for location in performance.original_locations.all()]

    if countries:
        country = countries[0]

    number = random.randrange(1, country_image_counts.get(country, country_image_counts["guinea"]) + 1)
    if country not in country_image_counts:
        return "guinea-{}-thumb.jpg".format(number)

    return "{}-{}-thumb.jpg".format(country, number)


@register.filter
def unique(value):
    return list({v for v in value})


@register.simple_tag
def replace_query_param(request, param, new_value):
    query_params = request.GET.copy()

    query_params[param] = new_value

    return query_params.urlencode()


@register.inclusion_tag("pagination/middle_pagination.html")
def build_pagination(page, request):
    ellipsis = ""
    number_of_initial_pages_to_show = 4
    current_page = page.number
    last_page = page.paginator.num_pages

    pages_to_show = []

    is_at_first_few_pages = current_page < number_of_initial_pages_to_show and current_page <= last_page
    if is_at_first_few_pages:
        max_sequential_pages_to_show = min([last_page, number_of_initial_pages_to_show])
        for page_num in range(1, max_sequential_pages_to_show + 1):
            pages_to_show.append(page_num)
        if last_page == number_of_initial_pages_to_show + 1:
            pages_to_show.append(last_page)
        elif last_page > number_of_initial_pages_to_show + 1:
            pages_to_show.append(ellipsis)
            pages_to_show.append(last_page)

    else:
        previous_page = current_page - 1
        pages_to_show.extend([1, ellipsis, previous_page, current_page])

        if current_page != last_page:
            pages_to_show.append(current_page + 1)
        if current_page < last_page - 2:
            pages_to_show.append(ellipsis)
        if current_page != last_page:
            pages_to_show.append(last_page)

    return {"pages_to_show": pages_to_show, "current_page": current_page, "request": request}



country_image_counts = {
    "back-cover-leo-in-studio": 1,
    "burkina-faso": 3,
    "chad": 1,
    "cote-d'ivore": 2,
    "countries-represented-by-works": 1,
    "coverpage": 1,
    "ethiopia": 1,
    "ghana": 5,
    "guinea": 20,
    "lesotho": 2,
    "liberia": 10,
    "mali": 3,
    "morocco": 1,
    "niger": 4,
    "nigeria": 5,
    "senegal": 2,
    "somalia": 1,
    "south-africa": 2,
    "tanzania": 2,
    "tunisia": 3,
    "upper-volta": 2,
    "zambia": 3
}
