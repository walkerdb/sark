from __future__ import absolute_import
import json

from django.urls import reverse
from django.db.models import Count
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.shortcuts import render
from django.template.loader import get_template
from haystack.views import FacetedSearchView

from ..sark import models as m


def about(request):
    selected = "about"
    t = get_template("about.html")
    html = t.render({'location_counts': selected})
    return HttpResponse(html)


def map(request):
    annotated_locations = m.Location.objects.filter(normalized_location=None).annotate(recording_count=Count("performance")).order_by("-recording_count", "country")

    location_counts = {}
    for loc in annotated_locations:
        location_counts[loc.country] = location_counts.get(loc.country, 0) + loc.recording_count

    t = get_template("map.html")
    html = t.render({'location_counts': json.dumps(location_counts)})

    return HttpResponse(html)


def contact_us(request):
    return HttpResponse(get_template("contact-us.html").render())


def inventory(request):
    selected = "inventory"
    t = get_template("inventory.html")
    html = t.render({'selected': selected})
    return HttpResponse(html)


def broadcast(request, year, month, day):
    selected = "demo"

    show = get_object_or_404(m.RadioShow, date="{0}-{1}-{2}".format(year, month, day))
    broadcast = show.performances.all()[0]

    performers = m.Agent.objects.filter(associatedagent__recording=broadcast, associatedagent__roles__role="Performer").all()
    # performers = [agent.agent for agent in broadcast.associated_agents_set.all() if performer in agent.roles.all()]

    script_pages = show.script_images.order_by('file').all()

    t = get_template("broadcast/broadcast.html")
    html = t.render({
        'selected': selected,
        'show': show,
        'broadcast': broadcast,
        'performers': performers,
        'script_pages': script_pages
    })
    return HttpResponse(html)


def field_recording(request, unique_id):
    selected = "demo"

    recording = get_object_or_404(m.FieldRecording, unique_id=unique_id)
    performances = recording.performances.all().order_by('audio')
    images = recording.images.all()

    t = get_template("fieldrecording.html")
    html = t.render({
        'selected': selected,
        'recording': recording,
        'performances': performances,
        'images': images
    })
    return HttpResponse(html)


def location(request, city, country):
    selected = "demo"
    google_web_api_string = "https://www.google.com/maps/embed/v1/place?zoom={0}&q={1}+{2}&key=AIzaSyDzDeB74FnjIvGAQhApW_8HVfrJSNq-nrE"

    if city:
        location = get_object_or_404(m.Location, city=city, country=country)
        people = m.Agent.objects.filter(primary_place_of_activity_id=location.pk)
        recordings = m.RadioShow.objects.filter(performances__normalized_locations=location.pk).order_by('title')
        google_web_api_string = google_web_api_string.format(9, location.city, location.country)
        location_string = str(location)

    else:
        people = m.Agent.objects.filter(primary_place_of_activity__country=country)
        recordings = m.RadioShow.objects.filter(performances__normalized_locations__country=country).order_by('title')
        google_web_api_string = google_web_api_string.format(6, "", country)
        location_string = country

    t = get_template("location.html")

    html = t.render({
        'selected': selected,
        'google_maps_api_link': google_web_api_string,
        'location': location_string,
        'people': people,
        'recordings': recordings
    })
    return HttpResponse(html)


def agent(request, name):
    selected = "demo"
    agent = get_object_or_404(m.Agent, name=name)
    radio_shows = m.RadioShow.objects.filter(performances__associated_agents=agent).order_by("date")
    # field_recordings = m.FieldRecording.objects.filter(performances__associated_agents__agent__id=agent.pk).distinct().order_by(
    #     "date")
    recorded = m.FieldRecording.objects.filter(recording_engineer__id=agent.pk)
    recordings = list(radio_shows) + list(recorded)
    members = agent.members.all()

    t = get_template("agent.html")
    html = t.render({
         'agentdata': agent,
         'recordings': recordings,
         'selected': selected,
         'members': members
    })

    return HttpResponse(html)


class SarkSearch(FacetedSearchView):
    def extra_context(self):
        context = super(SarkSearch, self).extra_context()
        context = self.remove_empty_facets(context)

        columns_per_row = 4
        row_length = 12

        context["columns_per_row"] = columns_per_row
        context["row_size"] = row_length // columns_per_row

        return context

    def get_results(self):
        search = self.form.search()

        return search

    def remove_empty_facets(self, context):
        new_context = context.copy()
        try:
            for field in new_context['facets']['fields'].keys():
                new_context['facets']['fields'][field] = self.filter_facets_with_no_results(new_context, field)
        except KeyError:
            pass
        return new_context

    @staticmethod
    def filter_facets_with_no_results(context, field):
        counts = context['facets']['fields'][field]
        return [(facet, count) for facet, count in counts if count > 0]


def model_list(request, model):
    if "locations" in model:
        objects = m.Location.objects.all()
        headers = ["Country", "City / Area"]
        data = []
        for location in objects:
            try:
                url = reverse('sark_django.sark.views.location',
                              kwargs={'country': location.country, 'name': location.name})
                cell_1_data = [url, location.country]
                cell_2_data = [url, location.name]
                data.append([cell_1_data, cell_2_data])
            except:
                print("womp")

    elif "broadcasts" in model:
        objects = m.RadioShow.objects.all()
        headers = ["Show", "Host", "Date"]
        data = []
        for show in objects:
            try:
                year, day, month = str(show.date).split("-")
                broadcast_url = reverse('sark_django.sark.views.broadcast',
                                        kwargs={'year': year, 'month': month, 'day': day})
                host_url = reverse('sark_django.sark.views.agent', args=(show.host.name,))

                cell_1_data = [broadcast_url, show.title]
                cell_2_data = [host_url, show.host.name]
                cell_3_data = ["", str(show.date_text)]
                data.append([cell_1_data, cell_2_data, cell_3_data])
            except:
                print("womp")
    elif "fieldrecordings" in model:
        objects = m.FieldRecording.objects.all()
        headers = ["Title", "Location", "Date"]
        data = []
        for recording in objects:
            try:
                date = str(recording.date_text)
                recording_url = reverse('sark_django.sark.views.field_recording', args=(recording.unique_id,))
                location_url = reverse('sark_django.sark.views.location',
                                       kwargs={'country': recording.location.country, 'name': recording.location.name})

                cell_1_data = [recording_url, recording.title]
                cell_2_data = [location_url, recording.location]
                cell_3_data = ["", date]
                data.append([cell_1_data, cell_2_data, cell_3_data])
            except:
                print("womp")

    elif "performance" in model:
        pass
    elif "agent" in model:
        pass

    t = get_template('model_list.html')
    html = t.render({
        'headers': headers,
        'data': data
    })
    return HttpResponse(html)


def error404(request, exception):
    return render(request, '404.html')
