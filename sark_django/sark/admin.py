from django.contrib import admin
from . import models as m


@admin.register(m.RadioShow)
class RadioShowAdmin(admin.ModelAdmin):
    fields = (
        ("title", "is_copyright_protected"),
        "barcode",
        "host",
        "description",
        "date",
        "date_text",
        "completeness",
        "script_text",
        "script_images",
        "performances"
    )
    readonly_fields = ("barcode",)
    date_hierarchy = 'date'
    search_fields = ["title"]
    filter_horizontal = ('script_images', "performances")

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "host":
            kwargs["queryset"] = m.Agent.objects.filter(associatedagent__roles__role="Host").distinct()
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


@admin.register(m.AssociatedAgent)
class AssociatedAgentAdmin(admin.ModelAdmin):
    search_fields = ["agent__name", "recording__title", "roles__role"]
    filter_horizontal = ('roles',)


@admin.register(m.Performance)
class PerformanceAdmin(admin.ModelAdmin):
    exclude = ["instruments", "genres", "photos", "date_accuracy"]
    date_hierarchy = 'date'
    filter_horizontal = ('associated_agents', 'original_locations', 'normalized_locations')
    search_fields = ["title", "date_text"]


@admin.register(m.Agent)
class AgentAdmin(admin.ModelAdmin):
    search_fields = ["name"]
    filter_horizontal = ('members', 'photos')


@admin.register(m.Location)
class LocationAdmin(admin.ModelAdmin):
    pass


@admin.register(m.Role)
class RoleAdmin(admin.ModelAdmin):
    pass

