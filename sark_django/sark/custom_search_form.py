from haystack.forms import FacetedSearchForm


class ReturnAllIfNothingFoundFacetedSearchForm(FacetedSearchForm):
    def no_query_found(self):
        return self.searchqueryset.all()
