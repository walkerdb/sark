"""sarkisian_django URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from __future__ import absolute_import
from datetime import date

from django.urls import path, re_path
from django.contrib import admin
from haystack.query import SearchQuerySet

from .sark.models import RadioShow
from .sark.custom_search_form import ReturnAllIfNothingFoundFacetedSearchForm
from .sark import views

app_name = "sark"

sqs = SearchQuerySet()\
    .date_facet('date', start_date=date(1955, 1, 1), end_date=date(2017, 1, 1), gap_by="year") \
    .facet("is_copyright_protected")\
    .facet("host")\
    .facet("performers")\
    .facet("faceted_model_type")\
    .facet("locations")\
    .facet("completeness")

urlpatterns = [
    path("", views.SarkSearch(
            form_class=ReturnAllIfNothingFoundFacetedSearchForm,
            searchqueryset=sqs.models(RadioShow).order_by("date"),
            template='home.html',
            results_per_page=28
        ),
        name='home'
    ),
    path('inventory/', views.inventory, name="inventory"),
    path('about/', views.about, name="about"),
    path('map/', views.map, name="map"),
    path('contact-us/', views.contact_us, name="contact-us"),
    path('broadcast/<year>-<month>-<day>/', views.broadcast, name="broadcast"),
    path('fieldrecording/<unique_id>', views.field_recording, name="field_recording"),
    re_path(r'^location/([^+]*)\+([^+]*)', views.location, name="location"),
    path('agent/<name>', views.agent, name="agent"),
    path('list/<model>', views.model_list, name="model_list"),
    path('admin/', admin.site.urls),
    path('search/', views.SarkSearch(
            form_class=ReturnAllIfNothingFoundFacetedSearchForm,
            searchqueryset=sqs,
            template='search/search.html'
        ),
        name='haystack_search'
    )
]

handler404 = 'sark_django.sark.views.error404'
admin.site.site_header = 'Sarkisian Project admin'
