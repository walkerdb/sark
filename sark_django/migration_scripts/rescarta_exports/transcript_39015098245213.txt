MUSIC TIME IN AFRICA I FEB. 18! 1979 PAGE ONE iVUICE: THE FOLLOWING PROGRAM IS MUSIC TIME IN AFRICA FOR BROADCAST 0N J â"âSUNDAY, FEBRUARY 18, 1979, AT 16 HOURS 35 AND 19 HOURS 35 GMT: â 15 SECS PAUSE: , #7 P 7 EVOICE: IT'S MUSIC TIME IN AFRICA: /Â¢Qwï¬yâ (2%ï¬f7r :NUSIC: THEME: 15 SECS, UNDER AND HOLD: gVOICE: AND HELLO AGAIN FRIENDS, I'M RITA ROCHELLE AND so GLAD YOU'RE : y JOINING NE TODAY FOR MUSIC TIME IN AFRICA. EACH WEEK NY COLLEAGUE TUâ LEO SARKISIAN AND I SELECT RECORDINGS 0F AUTHENTIC TRADTIONAL ' C AND POPUEER MUSIC FEATURING SOME OF AFRICAâS FINEST MUSICIANS AND CULTURAL GROUPS./ THE MUSIC7TODAX WILL BE FROM THE REPUBLIC OF AT THE END OF THE PROGRAM, SAY IVORY COAST AND/BEFORE SIGNING OFF XEEIX I'LL TRY TO/HELLO TO AS MANY OF YOU WHO HAVE WRITTEN TO US, AS TIME PERMITS. BUT RIGHT NOW, g ' STAY WITH ME FOR MUSIC TIME IN AFRICA: :MUSIC: THEME: UP, FOR 10 SECS, UNDER AND OUT: â ï¬g ; IVOICE: TEENISRRINEUNST THE REPUBLIC OF IVORY COAST IN WEST AFRICA HAS i I ABOUT 5 MILLION PEOPLE AND ABIDJAN THE MODERN INDUSTRIAL CAPITOL SS i 3 CITY HAS A POPULATION OF OVER A HALF MILLION. TEERE ARE MORE THAN i3 : â 60 LANGUAGES AND DDALECTS SPOKEN IN THE IVORY COAST AND SOME OF THE g 0 IMPORTANT ETHNIC GROUPS ARE THE SENUFO BAULE DAN, TN â I GOURO, BOBO, LOBI, MANDE, KROUMEN TAGOUNA AND DOUA. TEE $3 I IVORY COAST IS RE;RICR IN ITS CULTURAL AND ARTISTIC TRADITIONS - E ESPECIALLY IN TRE:VARIETY OF MUSICAL INSTRUMENTS, PERCUSSIVE Q3 I INSTRUMENTS AND THE REYTEEIC CEREMONIAL AND RITUAL DANCES. CUR %% ï¬ PROGRAM TODXX WILL BE THE XGKMXMKNEBKREEX MUSIC OF THE BAULE, 2 XEXREMIREMXEEETMENEMMEEEIMNMKEXMKE WHO NUMBER ABOUT ONE MILLION AND TI LIVE IN THE HEART OF THE COUNTRY, IN THE DEEP FORESTS AND SAVANNAH Â§Â§ REGIONS. IâM GOING TO BEGIN WITH A SELECTION THAT WAS RECORDED IN THE VILLAGE OF BRIKRO IN THE EASTERN PART OF BAULE COUNTRY. THE JIMINI

43Kâ. 0 âWin-ilk saga" . A. â .. .g: âm IMUSIC: EVOICEz' 'RUSIC: âVOICE: QMCSICL [MUSICâTIME IN AFRICA FEB. 18, 1979 PAGE Two SCENE IS THE OPEN PLACE IN THE CENTER OF THE VILLAGE WHERE A GROUP OF MORE THAN THIRTY SINGERS, MUSICIANS AND DANCERS HAVE GATHERED AND FORMED A LARGE CIRCLE. WHILE THE DANCERS PERFORM THEIR DANCE ROUTINE EN THEIR KNEES IN A CROUSHED POSITION, MUSICIANS PLAYING LARGE GOURD RATTLES SPIN THEIR INSTRUMENTS IN THE AIR AT THE SAME TIME. ADDING TO THE EXCITEMENT OF THE WHOLE AFFAIR ARE THE SOUNDS .OF AN ANTELOPE HORN: CUT ONE: h:10 AMONG THE BAULE, THERE ARE ININERANT MUSICIANS, BOTH PROFESSIONAL AND SEMIâPROFESSIONAL, WHO HHEMEHNEKEXMXHXMEEEMHN TRAVEL ABOUT THE COUNTRYSIDE SINGING THEIR PRAISE SONGS TO THE ACCOMPANIMENT OF A HARP. ACCORDING TO TRADITION THE MOST FAMOUS OF THESE ITINERANT MUSICIANS ARE SAID TO BE IN CONTACT WITH THE SPIRITS. THIS RECORDING IS BY AN ITINERANT BLIND MUSICIAN WHO CLAIMS THAT SOON AFTER HE HAD LOST HIS SIGHT,ZÂ§E SPIRITS ADVISED HIM TO LEARN HOW TO PLAY THE HARP AND THAT SINCE THEN HE HAS BEEN UNDER THE PROTECTION OF THE SPIRITS: CUT TWO: 3:08 AND NOW FROM THE VILLAGE OF POVEBO IN THE HEART OF BAULE COUNTRY, A GROUP OF MEN AND WOMEN PERFORM A DANCE KNOWN AS PANDA. WHILE DANCING, THE WOMEN CLAP THEIR HANDS AND THE MEN BEAT STICKS TOGETHER IN ACCOMPANIMENT TO THREE DRUMS, IRON BELLS, NEH CALABASH RATTLES AND A PAIR OF LARGE TALKING DRUMS PROPPED UP ON A WOODEN STAND. THE MUSICIAN PLAYING THE TALKING DRUMS ADDS A SHORT MUSICAL PHRASE AT THE END OF THE SONG, BRINGING THE DANCE TO A CLOSE: OUT THREE: 3:18 CB GD ID GD NE REESE CAREER g? 633 {23%

AVOICE: V wanâI â :1 AMUSIC: 1VOICE: v I âI X i .MUSIC: 'RUSIG TIME IN AFRICA _ FEBRHEHY 18, 1?79 PAGE THREE THAT WAS THE PANDA DANCE OF THE BAULE IN IVORY COAST THE SINGING WAS IN THE TYPICAL "CALL AND RESONSE" STYLE, WITH THE CHORAL GROUP ENTERING AT THE END OF EACH MUSICAL PHRASE SUNG THREE TIMES BY THE WOMEN, AND ALTERNATING WITH THE MALE VOCALISTS. ANOTHER EXCITING aw:ThMIEXEEHGNXHXEKHKEXNNHEN THE PERFORMANCE OF THE BAULE IS TRADTTTONAL DANCE OF THE ELEPHANT MASK - A CUSTOM WHTCH GOES BACK TO ANCIENT TIMES. THE HIGH QUALITY OF BAULE ART HAS BECOME WORLD FAMOUS SINCE COLLECTORS AND MANY MUSEUMS HAVE ACQUIRED BAULE MASKS. A MASK REPRESENTING AN ELEPHANT IS WORN BY A DANCER WHO,W ENTERS A CIRCLE OF MEN AND WOMEN, AS HE BEGINS TO DANCE, METAL BELLS ATTACHED TO HIS ANKLES JINGLE RHYTHMICALLY TO THE BEATS OF THE ACCOMPANYING DRUMS AND THE ENTIRE GROUP OF MEN AND WOMEN BEGINâ TO SING AND PLAY STICK RATTLES. HERE NOW IS THE MUSIC FOR THE EEEEHNNEXEXEKX DANCE OF THE ELEPHANT MASK OF THE BAULE: N . CUT FOUR: 3:35 â 'r CUSTOMS, TRADITION AND CULTURAL ACTIVITIES HAVE BEEN CHANGING JUST AS THE LIFE STYLES HID REGRETS SOCIAL ACTIVITIES OF PEOPLES ALL OVER THE WORLD HAVE BEEN CHANGING. CULTURAL CHANGES IN MUSIC ANDÂ» DANCE BECOME EVIDENT WHEN SPECIAL DANCES AND SONGS THAT WERE STRICTLY USED FOR CEREMONIAL AND RITUAL PRACTICES IN THEOLD DAYS, ARE Now PERFORMED ON ANY FESTIVE OCCASION FOR ENTERTAINMENT PURPOSES ONLY. THE CONGASSA MUSIC AND DANCES OF THE BAULE HERE ORIGINALLY LINKED WITH SECRET INTITIATION RITUALS NONADAYS THEY ARE PERFORMED JUST To HAVE A LOT OP FUN. THIS RECORDING WAS MADE IN THE BAULE VILLAGE OP CONGO-NOSSOU WHERE A HUGE GROUP OF SINGERS, MUSICIANS AND DANCERS WERE HAVING A GOOD TIME DOING THE CONGASSA. THE HIGHLIGHT OF THE WHOLE PERFORMANCE WAS A DANCER BRANDISHING A FLAMING TORCH SYMBOLIZING THE BRIGHT SHINING SUN: CUT FIVE: h:30 RSSSGSGOG TH I âWaaww i; T L a E. g, . I 31 I

âMUSIC TIME IN AFRICA i l :VOICE: l g ;NUSICL {VOICE: <,â,Â»-wv:ww~;Â» S... my...â . , H: at 1W3. .wv Q7â; iTHEME: FEB. 18, 1979 PAGE POUR ANOTHER MUSICAL INSTRUMENT USED BY THE BAULE MUSICIANS Is THE EAROUS MUSICAL BOW - SIMILAR To THOSE FOUND THROUGHOUT MOST OF SUB-SAHARA AFRICA. IN MOST WEST AFRICAN COUNTRIES, MUSICAL BOWS ARE JUST SIMPLE CURVED STICKS WITH ONE STRING MADE OF VEGETABLE FIBER. THE PLAYER PLACES THE STRING BETWEEN HIS LIPS AND STRIKES NEE IT WITH A THIN PIECE OF STICK HELD IN HIS RIGHT HAND. THE LEFT HAND PRESSES A SHORT PEECE OF WOOD AGAINST THE STRING TO CHANGE THE TONE AND CONTROL THE VIBRATIONS OF THE STRING. THE BAULE CALL THE MUSICAL BOW "GOH-DYEH" CUT SIX: Ã©zuo TODAY'S PROGRAM FEATURED THE MUSIC OF THE BAULE PEOPLE IN THE REPUBLIC OF IVORY COAST HOPE YOU LIKED THE RECORDINGS. I'LL BE BACK NEXT WEEK AT THIS SAME TIME AGAIN AND HOPE YOU'LL JOUN ME -- NAMES, ETC. ADDRESS(TWICE) THEN. RIGHT NOW, HERE'S HELLO TO: THIS IS RITA ROCHLLE SAYING'SO LONG TOR NOW AND INVITING YOU TO STAY TUNED IN FOR THE NEWS ON THE AFRICAN PROGRAM WHICH FOLLOWS IMMEDIATEEY ON THESE SAME FREQUENCIES AND METER BANDS. AND FROM OUR NUSICMAN LEO SARKISIAN AND ME, HEKMXERMXMEEHEH OUR VERY BEST WISHES THAT YOU ALL HAVE A NICE WEEK: UP TO TIME: AWL?

Jaw-Am v Â«ï¬g v 3 HEW ~ âWu-wâ ! :Â§:::M; ';5::::E:{::::;::;;;;:;:::222;:;;;;;;;;;;::;;;:;;::::f: â . â NAMES Pogmgggb 18, 1979 MUSIC TIME IN AFRICA PROGRAM: , r ~ [35%, âWâ ââ"f/Qpï¬*'â'~'*-â~--~â-~â.__. r i g HELLO TO: PEâER ISIGUZO, OF ENNxN ABA, IMO STATE, NIGERIA ï¬ , V -â /h j E STEVE ROfNiON, 0F ST.MARTIN'S IN NSAwAM, GHANA A Am: 4. g. ; .âGA3.ANÂ£$E OP ORLU, IMO STATE, THAT WASNICE OF YOU To ; ; A4 To WRITE AGAIN, G.CÂ¢3THANKS. '; A ~T â _ a x 1â ,âr/ 2â \\/YV\â0âQ g _ J (Â§*~Iâ A:) To NEEM ORIENT, 6E.AEIRPO, IMO STATE, NIOERIA,Â§ , ; V { TRANâYOUâEOR EEINO A REGULAR LISTENER AND WEyRE n g 5 ~ HAPPY THAT YOU LINED TEEPIOTURES OP LEO AND ME. 5 i WEâRE ALSO GLAD TO KNOW THAT YOU DO LIKE APRIOAN A i TRADITIONAL MUSIC As WELL As POPULAR MUSIC. p0 Â» Ã© , WRITE AGAIN. _ g" E i % 3 E .EHL KAGYA, 0F RUNASI, GHANA. IT WAS NICE HEARING Â» A 4 I PROM YOU TOO, AND GRAD THAT YOU ENJOY ALL OUR; ; â PROGRAMS. LOOKIIO FORWARD TO HEARING PROM YOU AGIN.. 7% x â waAVg,RIE, TO OUR LEAR FRIEED ONU ONwE, THANK YOU FOR IRE gÂ» A3(brï¬ L: NIQERQEE_NENSLETTER - IT'S A VERY NINE PUBLICATION 3 1 AND EOTE LEO AND I HAVE ENJOYED READING IT. OUR a a VERY BEST To YOU AND ALL YOUR COâWORKERS THEREEIN ; TEE COMPANY. AT 3i . _ I AÂ» 7 \ \râ a \â , V â â â f , . , ï¬/LL :31 O / M" j T <9 , . - i 1* â " I" â CLâ MW...Â»- 4-.. l _ ___/ââ~~_-W , 42â â . ,- â ?. i i 4 It}. / [9/ g? | . a â Prï¬k g 0 ,3