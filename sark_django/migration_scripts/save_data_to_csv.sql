Copy (
    SELECT sark_agent.name, count(sark_associatedagent)
    FROM sark_agent
    INNER JOIN sark_associatedagent ON sark_associatedagent.agent_id=sark_agent.id
    GROUP BY sark_agent.name
    ORDER BY sark_agent.name
) To '/tmp/people.csv' With CSV DELIMITER ',';


Copy (
    SELECT sark_location.country, sark_location.city, count(sark_performance_locations)
    FROM sark_location
    INNER JOIN sark_performance_locations ON sark_performance_locations.location_id=sark_location.id
    GROUP BY (sark_location.country, sark_location.city)
    ORDER BY sark_location.country
) To '/tmp/locations.csv' With CSV DELIMITER ',';
