import os

from tqdm import tqdm
from pprint import pprint

from sark_django.migration_scripts.parsers.audio_modsparser import AudioModsParser
from sark_django.migration_scripts.parsers.script_modsparser import ScriptModsParser
from sark_django.sark import models as m

PATH_TO_THIS_FILE = os.path.dirname(os.path.abspath(__file__))


class Importer:
    def __init__(self, metadata_path):
        self.mods_parser = self.get_parser_for_metadata_file(metadata_path)

    @staticmethod
    def get_parser_for_metadata_file(path_to_metadata_file):
        # if "miu00000_" in path_to_metadata_file:
        #     return AudioModsParser(path_to_metadata_file)

        return ScriptModsParser(path_to_metadata_file)

    def import_broadcast(self):
        recording = m.RadioShow.objects.get_or_create(
            host=self.get_agent(self.mods_parser.host()),
            title=self.mods_parser.title(),
            date=self.mods_parser.date_issued(),
            date_text=self.mods_parser.date_issued(),
            script_text=self.mods_parser.script(),
            completeness=self.mods_parser.completeness(),
            barcode=self.mods_parser.get_authoritative_identifier()
        )[0]

        recording.save()
        recording.script_images.set(self.build_script_images())

        performance = self.build_performance()
        if performance:
            recording.performances.set([performance])

    def build_script_images(self):
        identifier = self.mods_parser.get_authoritative_identifier()
        images = []
        for i in range(self.mods_parser.script_image_count()):
            path = "img/{}-script-{:02d}.jpg".format(identifier, i + 1)
            thumb_path = "img/{}-script-{:02d}-thumb.jpg".format(identifier, i + 1)
            try:
                image = m.Image.objects.get_or_create(file=path, thumb=thumb_path)[0]
                images.append(image)
            except Exception as e:
                print(e)
                print("skipping image; continuing")

        return images

    def build_performance(self):
        try:
            performance = m.Performance.objects.get_or_create(
                title=self.mods_parser.title(),
                date=self.mods_parser.date_issued(),
                date_text=self.mods_parser.date_issued(),
                audio="audio/" + self.mods_parser.get_authoritative_identifier() + ".mp3"

            )[0]
            locations = [
                m.Location.objects.get_or_create(city=city, country=country)[0]
                for country, city in self.mods_parser.locations_set()
            ]
            self.set_associated_agents(performance)
            performance.original_locations.set(locations)

            return performance
        except:
            return ""

    def set_associated_agents(self, performance):
        for person_roles in self.mods_parser.people_roles():
            role_strings = person_roles["roles"]
            name = person_roles["name"]
            roles = [m.Role.objects.get_or_create(role=role)[0] for role in role_strings]
            agent = self.get_agent(name)
            associated_agent = performance.associated_agents.through.objects.create(agent=agent, recording=performance)
            associated_agent.roles.set(roles)

    def get_people_from_db(self):
        people = []
        for person_role in self.mods_parser.people_roles():
            person = m.Agent.objects.get_or_create(name=person_role["name"])[0]
            roles = [m.Role.objects.get_or_create(role=role)[0] for role in person_role["roles"]]
            people.append((person, roles))
        return people

    @staticmethod
    def get_agent(name):
        matching_agent = m.Agent.objects.filter(name=name)
        if len(matching_agent) > 0:
            return matching_agent[0]
        else:
            return m.Agent.objects.get_or_create(name=name)[0]


def import_mods_data(root_path=os.path.join(PATH_TO_THIS_FILE, "rescarta_exports")):
    print(root_path)
    data_paths = [d for d in os.listdir(root_path) if (
            not d.startswith(".")
            and (os.path.isfile(os.path.join(root_path, d)) and d.endswith(".xml"))
    )]

    for path in tqdm(data_paths):
        metadata_path = os.path.join(root_path, path)
        try:
            Importer(metadata_path).import_broadcast()
        except Exception as e:
            print("failed to parse {}: {}".format(metadata_path, e))


def import_gold_recordings(root_path=os.path.join(PATH_TO_THIS_FILE, "gold_recordings")):
    xmls = set()
    for root, dirs, files in os.walk(root_path):
        xmls.update({os.path.join(root, f) for f in files if f.endswith(".xml")})

    for path in tqdm(list(xmls)):
        try:
            Importer(path).import_broadcast()
        except Exception as e:
            print("failed to parse {}: {}".format(path, e))


if __name__ == "__main__":
    import_gold_recordings()
