from sark_django.migration_scripts.processors.downsize_images import ImageProcessor
from sark_django.migration_scripts.processors.resample_audio import AudioProcessor
from sark_django.migration_scripts.processors.metadata_extractor import MetadataExtractor
import os

RAW_DATA_PATH = "/Volumes/MTIA/Kaltura/"
# PATH_TO_WRITE_TO = "/Users/walkerdb/PycharmProjects/sark/sark_django/static/sarkfiles"
PATH_TO_WRITE_TO = "/Users/walkerdb/PycharmProjects/sark/sark_django/static/sarkfiles"

PATH_TO_THIS_FILE = os.path.dirname(os.path.abspath(__file__))
RESCARTA_DATA_PATH = os.path.join(PATH_TO_THIS_FILE, "rescarta_exports")


def main():
    print("processing metadata")
    MetadataExtractor(RAW_DATA_PATH, RESCARTA_DATA_PATH).copy_metadata_files_and_extract_transcriptions()

    print("processing images")
    ImageProcessor(RAW_DATA_PATH, os.path.join(PATH_TO_WRITE_TO, "img")).create_images()

    print("processing audio")
    AudioProcessor(RAW_DATA_PATH, os.path.join(PATH_TO_WRITE_TO, "audio")).resample_audio()


if __name__ == "__main__":
    main()
