import subprocess
import os
import re

from tqdm import tqdm
from shutil import copy2

from sark_django.migration_scripts.processors.base import BaseProcessor


class MetadataExtractor(BaseProcessor):

    def copy_metadata_files_and_extract_transcriptions(self):
        leaf_nodes = self.get_leaf_node_dirs()
        for leaf_node in tqdm(leaf_nodes):
            entity_id = self.get_identifier_from_metadata_file(leaf_node)

            source_metadata_path = os.path.join(leaf_node, "metadata.xml")
            if os.path.isfile(source_metadata_path):
                target_metadata_path = os.path.join(self.PATH_TO_WRITE_TO, "metadata_{}.xml".format(entity_id))
                copy2(source_metadata_path, target_metadata_path)
            else:
                print("no metadata found for entity {}!".format(entity_id))

            texts = [self.extract_text_from_metadata_file(leaf_node)]

            if not texts:
                print("no trascriptions found in metadata; defaulting to look at tiff images")
                texts = self.extract_text_from_tiff_files(leaf_node)

            if not texts:
                print("broadcast {} has no transcription".format(entity_id))
                continue

            with open(os.path.join(self.PATH_TO_WRITE_TO, "transcript_{}.txt".format(entity_id)), mode="w") as f:
                f.write("\n\n".join(texts))

    def extract_text_from_tiff_files(self, leaf_node):
        tiffs = sorted([f for f in os.listdir(leaf_node) if f.endswith(".tif") or f.endswith(".tiff")])

        return list(filter(None, [self.extract_text(os.path.join(leaf_node, tiff)) for tiff in tiffs]))

    def extract_text_from_metadata_file(self, leaf_node):
        metadata = self.get_metadata(leaf_node)
        return metadata.script() if metadata else ""

    def consolidate_folders(self):
        script_leaf_nodes = self.get_leaf_node_dirs()
        for leaf in script_leaf_nodes:
            entity_id = self.get_identifier_from_metadata_file(leaf)
            if not os.path.isdir(os.path.join(self.PATH_TO_WRITE_TO, entity_id)):
                print("not a directory! {} - {}".format(leaf, entity_id))

            files_to_copy = os.listdir(leaf)
            for f in files_to_copy:
                target = os.path.join(self.PATH_TO_WRITE_TO, entity_id, f)
                source = os.path.join(leaf, f)
                copy2(source, target)

    def get_identifiers_in_folder(self):
        return [[self.get_identifier_from_metadata_file(leaf)] for leaf in tqdm(self.get_leaf_node_dirs())]



    @staticmethod
    def extract_text(filepath):
        output = subprocess.check_output(["tiffinfo", filepath]).decode('utf-8')
        regex = re.compile("Tag 5688: ([0-9,]*)")
        match = re.findall(regex, output)
        if not match:
            print("didn't find any transcription in the tiff metadata")
            return

        ints = eval("[{}]".format(match[0]))
        text = "".join([chr(num) for num in ints]).replace("\n", " ")

        return text


if __name__ == "__main__":
    source_path = "/Volumes/MTIA/Kaltura/Gold_DONE_NoScripts"
    path_to_this_file = os.path.dirname(os.path.abspath(__file__))
    target_path = os.path.join(path_to_this_file, "../rescarta_exports")
    # MetadataExtractor(source_path, target_path).copy_metadata_files_and_extract_transcriptions()
    import csv
    with open("GOLD_BROADCASTS.csv", mode="w") as f:
        writer = csv.writer(f)
        writer.writerows(MetadataExtractor(source_path, target_path).get_identifiers_in_folder())
