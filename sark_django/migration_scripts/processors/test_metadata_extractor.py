from sark_django.migration_scripts.processors.metadata_extractor import MetadataExtractor


class TestClass:
    def test_can_extract_text(self):
        extractor = MetadataExtractor("", "")
        text = extractor.extract_text("./test_tiff_with_transcription.tif")

        assert "MUSIC TIME" in text

    def test_cleans_up_unicode_leftovers(self):
        extractor = MetadataExtractor("", "")
        text = extractor.extract_text("./test_tiff_with_transcription.tif")

        assert "\\x" not in text
