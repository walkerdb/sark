import os

from sark_django.migration_scripts.parsers.audio_modsparser import AudioModsParser
from sark_django.migration_scripts.parsers.script_modsparser import ScriptModsParser


class BaseProcessor:
    def __init__(self, raw_data_path, path_to_write_to):
        self.RAW_DATA_PATH = raw_data_path
        self.PATH_TO_WRITE_TO = path_to_write_to

    def get_identifier_from_metadata_file(self, path_to_leaf):
        metadata = self.get_metadata(path_to_leaf)
        return metadata.get_authoritative_identifier() if metadata else "unknown"

    def get_metadata(self, path_to_leaf):
        metadata_path = os.path.join(self.RAW_DATA_PATH, path_to_leaf, "metadata.xml")
        if os.path.isfile(metadata_path):
            return ScriptModsParser(metadata_path)

        xml_files = [f for f in os.listdir(os.path.join(self.RAW_DATA_PATH, path_to_leaf)) if f.endswith(".xml")]
        if not xml_files:
            return

        metadata_path = os.path.join(self.RAW_DATA_PATH, path_to_leaf, xml_files[0])
        if os.path.isfile(metadata_path):
            return AudioModsParser(metadata_path)

        return

    def get_leaf_node_dirs(self):
        paths = []
        for dirpath, dirnames, filenames in os.walk(self.RAW_DATA_PATH):
            if not dirnames and "/." not in dirpath:  # this is a leaf in the directory structure
                paths.append(dirpath)
        return paths
