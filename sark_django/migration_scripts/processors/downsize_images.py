import os

from PIL import Image
from tqdm import tqdm

from sark_django.migration_scripts.processors.base import BaseProcessor


class ImageProcessor(BaseProcessor):
    def create_images(self):
        paths = self.get_leaf_node_dirs()

        for path_to_leaf in tqdm(paths):
            self.process_images(path_to_leaf)

    def process_images(self, path_to_leaf):
        identifier = self.get_identifier_from_metadata_file(path_to_leaf)
        files = os.listdir(os.path.join(self.RAW_DATA_PATH, path_to_leaf))
        images = [f for f in files if f.endswith(".tif") and not f.startswith(".")]
        images.sort()
        for index, image_filename in enumerate(images):
            image_path = os.path.join(self.RAW_DATA_PATH, path_to_leaf, image_filename)
            try:
                self.create_derivative_images(identifier, image_path, index)
            except Exception as e:
                print("failed to convert image: ", image_path, index, e)

    def create_derivative_images(self, identifier, image_path, index):
        image = Image.open(image_path)
        page = index + 1
        target_img_path = os.path.join(self.PATH_TO_WRITE_TO, "{}-script-{:02d}.jpg".format(identifier, page))
        target_thumb_path = os.path.join(self.PATH_TO_WRITE_TO, "{}-script-{:02d}-thumb.jpg".format(identifier, page))
        if os.path.isfile(target_img_path) and os.path.isfile(target_thumb_path):
            return

        # image.save(os.path.join(static_path, "{}-script-{:02d}-master.png".format(barcode, page)))
        thumb = image.copy()
        thumb.thumbnail((300, 300), Image.ANTIALIAS)
        thumb.save(target_thumb_path)
        image.thumbnail((1500, 1500), Image.ANTIALIAS)
        image.save(target_img_path)


if __name__ == "__main__":
    RAW_DATA_PATH = "/Volumes/MTIA/Kaltura"
    PATH_TO_WRITE_TO = "/Volumes/MirrorrorriM/sark/img"
    ImageProcessor(RAW_DATA_PATH, PATH_TO_WRITE_TO).create_images()
