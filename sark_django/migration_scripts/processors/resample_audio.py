import os

from subprocess import run, DEVNULL, PIPE, STDOUT
from tqdm import tqdm

from sark_django.migration_scripts.processors.base import BaseProcessor


class AudioProcessor(BaseProcessor):
    def resample_audio(self):
        paths = self.get_leaf_node_dirs()

        for path_to_leaf in tqdm(paths):
            identifier = self.get_identifier_from_metadata_file(path_to_leaf)

            audio_files = [f for f in os.listdir(os.path.join(self.RAW_DATA_PATH, path_to_leaf)) if f.endswith(".mp3") and not f.startswith(".")]
            audio_files.sort()

            for index, audio_filename in enumerate(audio_files):
                audio_path = os.path.join(self.RAW_DATA_PATH, path_to_leaf, audio_filename)
                try:
                    self.create_derivative_audio(identifier, audio_path)
                except Exception as e:
                    print("failed to convert audio: ", audio_path, index, e)

    def create_derivative_audio(self, identifier, source_path):
        write_path = os.path.join(self.PATH_TO_WRITE_TO, identifier + ".mp3")
        if os.path.isfile(write_path):
            return

        result = run(["ffmpeg", "-y", "-i", source_path, "-ab", "128k", write_path], stdout=DEVNULL, stdin=PIPE, stderr=STDOUT)

        if result.returncode:  # the code for success (0) is falsey
            print("failed to convert audio file at ", source_path)


if __name__ == "__main__":
    RAW_DATA_PATH = "/Volumes/MTIA-1/Kaltura"
    PATH_TO_WRITE_TO = "/Volumes/MirrorrorriM/sark/audio"
    AudioProcessor(RAW_DATA_PATH, PATH_TO_WRITE_TO).resample_audio()
