import os
import csv
from pprint import pprint

from sark_django.migration_scripts.parsers.audio_modsparser import AudioModsParser
from sark_django.migration_scripts.parsers.script_modsparser import ScriptModsParser


if __name__ == "__main__":
    data_path = "/Users/walkerdb/PycharmProjects/sark/sark_django/migration_scripts/rescarta_exports"
    files = [f for f in os.listdir(data_path) if f.endswith(".xml")]

    extant_dates = set()
    for file in files:
        path = os.path.join(data_path, file)
        with open(path) as f:
            data = f.read()
            if 'TYPE="AUDIO RECORDING"' in data:
                metadata = AudioModsParser(path)
            else:
                metadata = ScriptModsParser(path)
        extant_dates.add(metadata.date_issued())

    with open("/Users/walkerdb/PycharmProjects/sark/sark_django/migration_scripts/metadata_cleanup/mtia_master_data.csv") as f:
        reader = csv.DictReader(f)
        expected_dates = {r["ISO Date"] for r in reader}

    missing_broadcasts = expected_dates.difference(extant_dates)
    pprint(missing_broadcasts)
    print(len(missing_broadcasts))
    print(len(extant_dates))
