from lxml import etree


class XMLReader:

    def __init__(self, path):
        self.path = path
        self.tree = etree.parse(path)
        self.root = "/mets:dmdSec/mets:mdWrap/mets:xmlData/"
        self.namespaces = {
            "mods": "http://www.loc.gov/mods/v3",
            "mets": "http://www.loc.gov/METS/",
            "xsi": "http://www.w3.org/2001/XMLSchema-instance",
            "xlink": "http://www.w3.org/1999/xlink",
            "marc21": "http://www.loc.gov/MARC21/slim",
            "rights": "http://cosimo.stanford.edu/sdr/metsrights/",
            "aes": "http://www.aes.org/audioObject",
            "tcf": "http://www.aes.org/tcf",
            "ph": "http://www.aes.org/processhistory",
            "dc": "http://purl.org/dc/elements/1.1",
            "fn": "http://www.w3.org/TR/xpath-functions/",
            "fits": "http://hul.harvard.edu/ois/xml/ns/fits/fits_output"
        }

    def get_elements_for_xpath(self, xpath):
        return self.tree.xpath(xpath, namespaces=self.namespaces)
