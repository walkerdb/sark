import os
from copy import deepcopy
import csv

from lxml import etree
from tqdm import tqdm

from sark_django.migration_scripts.metadata_cleanup.xml_reader import XMLReader


class Normalizer(XMLReader):

    def normalize_countries(self, normalization_map):
        countries = self.get_elements_for_xpath("//mods:country")
        for country in countries:
            original_text = country.text
            modern_text = normalization_map.get(original_text, {"modern": original_text}).get("modern")
            corrected_contemporary_text = normalization_map.get(original_text, {"normalized": original_text}).get("normalized")

            country.text = corrected_contemporary_text

            if corrected_contemporary_text != modern_text:
                parent_subject = country.getparent().getparent()
                copy = deepcopy(parent_subject)
                copy[0][0].text = modern_text
                parent_subject.getparent().append(copy)

        with open(self.path, "wb") as f:
            string = etree.tostring(self.tree, pretty_print=True, xml_declaration=True, encoding="utf-8")
            f.write(string)

        pass

    def extract_people(self):
        return self._generate_rows("//mods:namePart")

    def extract_countries(self):
        return self._generate_rows("//mods:country")

    def _generate_rows(self, xpath):
        elements = self.get_elements_for_xpath(xpath)
        return [self._generate_row_for_cleaning(e) for e in elements]

    def _generate_row_for_cleaning(self, e):
        return e.text, e.text, self.tree.getpath(e), self.path


if __name__ == "__main__":
    normalization_map = {}
    with open("mapped_nations.csv") as f:
        data = csv.DictReader(f)
        for row in data:
            key = row["Nation"]
            value = {
                "modern": row["Modern region"],
                "normalized": row["Normalized"]
            }
            normalization_map[key] = value

    results = []
    files = [f for f in os.listdir("../rescarta_exports") if f.endswith(".xml")]
    for file in tqdm(files):
        Normalizer(os.path.join("../rescarta_exports", file)).normalize_countries(normalization_map)
    #     results.extend(Normalizer(os.path.join("../rescarta_exports", file)).extract_countries())
    #
    # import csv
    # with open("countries.csv", "w") as f:
    #     writer = csv.writer(f)
    #     writer.writerow(["name", "name_normalized", "xpath", "filepath"])
    #     writer.writerows(results)
    #     # f.write(json.dumps(Counter(places).most_common(), indent=4))

