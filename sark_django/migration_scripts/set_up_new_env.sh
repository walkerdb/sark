sudo mkdir -p /webApps
sudo chown -R "$USER": /webApps

mkdir -p /webApps/sark_media
mkdir -p /webApps/sark_media/img
mkdir -p /webApps/sark_media/audio

cd /webApps
git clone https://gitlab.com/walkerdb/sark.git

mkdir -p /webApps/sark/sark_django/static/sarkfiles
mkdir -p /webApps/sark/sark_django/static/sarkfiles/img
mkdir -p /webApps/sark/sark_django/static/sarkfiles/audio

ln -s /webApps/sark_media/img/* ./sarkfiles/img/
ln -s /webApps/sark_media/audio/* ./sarkfiles/audio/