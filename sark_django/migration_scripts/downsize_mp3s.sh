#!/usr/bin/env bash
for name in **/*.mp3 ; do
    x=$(cut -d"_" -f1 <<<"${name}")
    id_number=$(cut -d"/" -f2 <<<"${x}")
    ffmpeg -i "${name}" -ab 128k "/Users/walkerdb/PycharmProjects/sark/sark_django/static/sarkfiles/audio/${id_number}.mp3"
done
