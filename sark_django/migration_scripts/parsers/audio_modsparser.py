from sark_django.migration_scripts.parsers.baseparser import BaseParser


class AudioModsParser(BaseParser):
    def __init__(self, path):
        root = "/mets:dmdSec/mets:mdWrap/mets:xmlData/"
        namespaces = {
            "mods": "http://www.loc.gov/mods/v3",
            "mets": "http://www.loc.gov/METS/",
            "xsi": "http://www.w3.org/2001/XMLSchema-instance",
            "xlink": "http://www.w3.org/1999/xlink",
            "marc21": "http://www.loc.gov/MARC21/slim",
            "rights": "http://cosimo.stanford.edu/sdr/metsrights/",
            "aes": "http://www.aes.org/audioObject",
            "tcf": "http://www.aes.org/tcf",
            "ph": "http://www.aes.org/processhistory",
            "dc": "http://purl.org/dc/elements/1.1",
            "fn": "http://www.w3.org/TR/xpath-functions/",
            "fits": "http://hul.harvard.edu/ois/xml/ns/fits/fits_output"
        }
        super(AudioModsParser, self).__init__(path, root, namespaces)

    def barcode(self):
        return self.find("dc:identifier").text

    def completeness(self):
        return "Gold"

    def date_issued(self):
        try:
            date = self.find("dc:date").text
        except Exception as e:
            print(self.path)
            raise e

        if "-" not in date:
            year = date[:4]
            month = date[4:6]
            day = date[6:8]

            if int(year) < 1940 or int(year) > 2017:
                print("something is wonky with the date for this recording.", "date:", date, "path", self.path)
                return ""

            date = "{}-{}-{}".format(year, month, day)

        return date

    def title(self):
        return "MTIA {}".format(self.date_issued())

    def publisher(self):
        return "Voice of America"

    def origin_location(self):
        return "Washington, D.C."

    def roles(self):
        return set()

    def locations_set(self):
        return set()

    def host(self):
        return ""

    def duration(self):
        pass

    def local_identifier(self):
        return ""

    def people_roles(self):
        return []

    def date_captured(self):
        pass
