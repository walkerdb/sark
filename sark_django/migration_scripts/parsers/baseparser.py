from abc import ABCMeta, abstractmethod
import os
from lxml import etree


class BaseParser:
    __metaclass__ = ABCMeta

    def __init__(self, path, root, namespaces):
        self.path = path
        self.tree = etree.parse(path)
        self.root = root
        self.namespaces = namespaces

    def get_authoritative_identifier(self):
        barcode = self.barcode()
        if barcode:
            return barcode

        local_id = self.local_identifier()
        if local_id:
            return local_id

        raise Exception("no identifier found for broadcast at " + self.path)

    def find(self, path):
        return self.tree.find(self.root + path, namespaces=self.namespaces)

    def findall(self, path):
        return self.tree.findall(self.root + path, namespaces=self.namespaces)

    def find_text_or_default(self, element, xpath):
        e = element.xpath(xpath, namespaces=self.namespaces)
        if not e:
            return ""

        return e[0].text

    def script(self):
        text_node = self.get_note("Transcript")
        if text_node:
            return text_node

        t_path = os.path.join(os.path.dirname(self.path), "transcript_{}.txt".format(self.get_authoritative_identifier()))

        if not os.path.isfile(t_path):
            print("no transcript found for script {}".format(self.path))
            return ""

        with open(t_path) as f:
            return f.read()

    def script_image_count(self):
        files = self.tree.xpath("//mets:FLocat", namespaces=self.namespaces) or []
        return len([f for f in files if "tif" in str(etree.tostring(f))])

    def get_note(self, note_type):
        try:
            return [note for note in self.findall("mods:note") if note.get("type") == note_type][0].text
        except IndexError as e:
            # print("no note found for type '{}': ".format(note_type), e)
            return ""

    @abstractmethod
    def title(self): raise NotImplementedError

    @abstractmethod
    def roles(self): raise NotImplementedError

    @abstractmethod
    def host(self): raise NotImplementedError

    @abstractmethod
    def duration(self): raise NotImplementedError

    @abstractmethod
    def completeness(self): raise NotImplementedError

    @abstractmethod
    def locations_set(self): raise NotImplementedError

    @abstractmethod
    def barcode(self): raise NotImplementedError

    @abstractmethod
    def local_identifier(self): raise NotImplementedError

    @abstractmethod
    def people_roles(self): raise NotImplementedError

    @abstractmethod
    def date_captured(self): raise NotImplementedError

    @abstractmethod
    def origin_location(self): raise NotImplementedError

    @abstractmethod
    def date_issued(self): raise NotImplementedError

    @abstractmethod
    def publisher(self): raise NotImplementedError




