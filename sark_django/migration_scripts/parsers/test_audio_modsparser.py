import os
from .audio_modsparser import AudioModsParser

BASE_PATH = os.path.dirname(os.path.abspath(__file__))


class TestClass(object):
    def test_parser_can_parse_test_xml(self):
        parser = AudioModsParser(os.path.join(BASE_PATH, "./test_data/test_audio_dc.xml"))

        assert parser.get_authoritative_identifier() == "39015098242657"
        assert parser.completeness() == "Gold"
        assert parser.date_issued() == '1993-05-16'
        assert parser.script_image_count() == 0
        assert parser.title() == "MTIA 1993-05-16"
        assert parser.host() == ""
        assert parser.publisher() == 'Voice of America'
        assert parser.origin_location() == 'Washington, D.C.'
        assert parser.roles() == set()
