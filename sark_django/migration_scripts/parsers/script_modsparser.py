from lxml import etree
import os

from sark_django.migration_scripts.parsers.baseparser import BaseParser


class ScriptModsParser(BaseParser):
    def __init__(self, path):
        root = "/mets:dmdSec/mets:mdWrap/mets:xmlData/mods:mods/"
        namespaces = {
            "mods": "http://www.loc.gov/mods/v3",
            "mets": "http://www.loc.gov/METS/",
            "mix": "http://www.loc.gov/mix/",
            "xlink": "http://www.w3.org/1999/xlink",
            "xsi": "http://www.w3.org/2001/XMLSchema-instance"
        }
        super(ScriptModsParser, self).__init__(path, root, namespaces)

    def title(self):
        return self.find("mods:titleInfo/mods:title").text

    def roles(self):
        people = self.findall("mods:name")
        roles = set()

        for element in people:
            person_roles = element.findall("mods:role/mods:roleTerm", namespaces=self.namespaces)
            roles |= {role.text for role in person_roles if role.get("type") == "text"}

        return roles

    def host(self):
        hosts = [p["name"] for p in self.people_roles() if "Host" in p["roles"]]
        if not hosts:
            return ""
        return hosts[0]

    def duration(self):
        return self.get_note("Duration")

    def completeness(self):
        return self.get_note("Completeness") or "Platinum"

    def location_dicts(self):
        locations = []
        location_nodes = self.findall("mods:subject/mods:hierarchicalGeographic")
        for location in location_nodes:
            country = self.find_text_or_default(location, "mods:country")
            city = self.find_text_or_default(location, "mods:city")
            locations.append({"country": country, "city": city})
        return locations

    def locations_set(self):
        return {(d.get("country"), d.get("city")) for d in self.location_dicts()}

    def barcode(self):
        barcode_nodes = [identifier for identifier in self.findall("mods:identifier") if identifier.get("type").lower() == "barcode"]
        if not barcode_nodes:
            return ""

        return barcode_nodes[0].text

    def local_identifier(self):
        local_id_nodes = [identifier for identifier in self.findall("mods:identifier") if identifier.get("type").lower() == "local"]
        if not local_id_nodes:
            return ""

        return local_id_nodes[0].text.replace("/", "_")

    def people_roles(self):
        people = self.findall("mods:name")
        people_formatted = []

        for person in people:
            name = person.find("mods:namePart", namespaces=self.namespaces).text
            person_roles = person.findall("mods:role/mods:roleTerm", namespaces=self.namespaces)
            roles = [role.text for role in person_roles if role.get("type") == "text"]
            people_formatted.append({"name": name, "roles": roles})

        return people_formatted

    def date_captured(self):
        origin_node = self.find("mods:originInfo")
        return origin_node.find("mods:dateCaptured", namespaces=self.namespaces).text

    def origin_location(self):
        origin_node = self.find("mods:originInfo")
        return origin_node.find("mods:place/mods:placeTerm", namespaces=self.namespaces).text

    def date_issued(self):
        origin_node = self.find("mods:originInfo")
        try:
            return origin_node.find("mods:dateIssued", namespaces=self.namespaces).text
        except Exception as e:
            print("script at " + self.path + " has no broadcast date")
            return ""

    def publisher(self):
        origin_node = self.find("mods:originInfo")
        return origin_node.find("mods:publisher", namespaces=self.namespaces).text
