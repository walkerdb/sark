import os
from pprint import pprint

from .script_modsparser import ScriptModsParser

BASE_PATH = os.path.dirname(os.path.abspath(__file__))


class TestClass(object):
    def test_parser_can_parse_test_xml(self):
        parser = ScriptModsParser(os.path.join(BASE_PATH, "./test_data/test_script_mods.xml"))

        assert parser.barcode() == "39015098235396"
        assert parser.local_identifier() == "miu00000_40190000_00000031"
        assert parser.get_authoritative_identifier() == "39015098235396"
        assert parser.host() == "Bryn Poole"
        assert parser.title() == "Music Time in Africa March 5 1967"
        assert parser.completeness() == "Platinum"
        assert parser.duration() == "30:32"
        assert parser.date_issued() == '1967-03-05'
        assert parser.date_captured() == '2017'
        assert parser.publisher() == 'Voice of America'
        assert parser.origin_location() == 'Washington, D.C.'
        assert parser.roles() == {'Author of dialog', 'Singer', 'Producer', 'Host', 'Creator', 'Owner', 'Instrumentalist', 'Performer'}
        assert parser.script_image_count() == 3

        assert ("Republic of the Congo", "Kinshasa") in parser.locations_set()
        assert "Africa" in parser.script()

    def test_parser_can_parse_test_gold_xml(self):
        parser = ScriptModsParser(os.path.join(BASE_PATH, "./test_data/test_gold_script_mods.xml"))

        assert parser.barcode() == "39015098243986"
        assert parser.local_identifier() == "miu00000_00000001_00000043"
        assert parser.get_authoritative_identifier() == "39015098243986"
        assert parser.host() == "Sue Moran"
        assert parser.title() == "Music Time in Africa January 5 1968"
        assert parser.completeness() == "Gold"
        assert parser.duration() == "31:31"
        assert parser.date_issued() == '1968-01-05'
        assert parser.date_captured() == '2017'
        assert parser.publisher() == 'Voice of America'
        assert parser.origin_location() == 'Washington, D.C.'
        assert parser.roles() == {'Author of dialog', 'Producer', 'Host', 'Creator', 'Owner'}
        assert parser.script_image_count() == 0

        assert ("Zambia", "") in parser.locations_set()
        assert parser.script() == ""
