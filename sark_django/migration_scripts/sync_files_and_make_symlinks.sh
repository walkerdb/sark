#!/usr/bin/env bash

remote_address=${SARK_REMOTE_ADDRESS}
remote_source_dir="/webApps/sark_media"
remote_symlink_dir="/webApps/sark/sark_django/static/sarkfiles"
local_dir="${HOME}/PycharmProjects/sark/sark_django/static/sarkfiles"

rsync -vrzh "${local_dir}/img/" "${remote_address}:${remote_source_dir}/img"
rsync -vrzh "${local_dir}/audio/" "${remote_address}:${remote_source_dir}/audio"

ssh $remote_address "ln -s /webApps/sark_media/img/* ${remote_symlink_dir}/img/"
ssh $remote_address "ln -s /webApps/sark_media/audio/* ${remote_symlink_dir}/audio/"