import csv
from sark_django.sark import models as m

m.Location.objects.order_by('date_modified').last().date_modified

with open("/sark/sark_django/migration_scripts/scratch/locations.csv") as f:
    data = csv.DictReader(f)
    for row in data:
        original_country = row["Country"].strip()
        authoritative_country = row["Authoritative Country"].strip()
        original_city = row["City"].strip()
        authoritative_city = row["Authoritative City"].strip()
        try:
            print("getting or creating authoritative location")
            authoritative_location = m.Location.objects.get_or_create(
                city=authoritative_city,
                country=authoritative_country,
            )[0]
            for performance in m.Performance.objects.filter(
                    original_locations=m.Location.objects.filter(country=original_country, city=original_city).first()
            ):
                performance.normalized_locations.add(authoritative_location)
            print("updated performance!")
        except Exception as error:
            print("failed to update location", original_country, original_city, authoritative_country, authoritative_city, error)

