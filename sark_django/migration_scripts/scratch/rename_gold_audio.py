import os
import re
from shutil import copyfile

if __name__ == "__main__":
    audio_dir = "/webApps/sark_temp"

    mp3s = set()
    imgs = set()
    for root, dirs, files in os.walk("/webApps/sark_temp"):
        mp3s.update({f for f in files if f.endswith(".mp3")})
        imgs.update({f for f in files if f.endswith(".JPG")})

    for mp3 in mp3s:
        match = re.search(r'(.+)_streaming\.mp3', mp3)
        barcode = match.group(1)
        old_path = os.path.join("/webApps/sark_temp", barcode, barcode + "_streaming.mp3")
        new_path = os.path.join("/webApps/sark_media/audio", barcode + ".mp3")
        copyfile(old_path, new_path)

    for jpg in imgs:
        match = re.search(r'(.+)_(.+)\.JPG', jpg)
        barcode = match.group(1)
        img_name = match.group(2)
        number = 1
        if len(img_name.split('-')) > 1:
            img_name, number = img_name.split('-')
            number = int(number)

        filename = "{}-{}-{:02d}.jpg".format(barcode, img_name, number)
        print(filename)
        old_path = os.path.join("/webApps/sark_temp", barcode, jpg)
        new_path = os.path.join("/webApps/sark_media/img", filename)
        copyfile(old_path, new_path)
