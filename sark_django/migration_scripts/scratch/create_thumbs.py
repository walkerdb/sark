import os
import re

from PIL import Image
from tqdm import tqdm

root_path = "/Users/walkerdb/Downloads/SarkisianFaces/PaintingCrops"


def make_thumb(image_path):
    filename = image_path.split("/")[-1].split(".")[0]
    country = re.search(r'([^\d.]*)', filename).groups()[0].lower().strip().replace(" ", "-")

    i = 1
    while os.path.isfile(os.path.join(root_path, "derivatives", "{}-{}-thumb.jpg".format(country, i))):
        i += 1


    thumb = Image.open(os.path.join(root_path, image_path))
    thumb.thumbnail((300, 300), Image.ANTIALIAS)
    thumb.save(os.path.join(root_path, "derivatives", "{}-{}-thumb.jpg".format(country, i)))


if __name__ == "__main__":
    # if not os.path.exists(os.path.join(root_path, "derivatives")):
    #     os.makedirs(os.path.join(root_path, "derivatives"))
    #
    # for image in tqdm([i for i in os.listdir(root_path) if i.endswith(".jpg")]):
    #     make_thumb(image)

    exports_dir = "/Users/walkerdb/PycharmProjects/sark/sark_django/migration_scripts/rescarta_exports"
    for d in [_ for _ in os.listdir(exports_dir) if os.path.isdir(_)]:
        txt_files = [f for f in os.listdir(os.path.join(exports_dir, d)) if f.endswith(".txt")]
        for txt_file in txt_files:
            os.rename(os.path.join(exports_dir, d, txt_file), os.path.join(exports_dir, txt_file))
