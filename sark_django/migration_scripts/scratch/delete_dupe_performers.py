from collections import defaultdict

from sark_django.sark import models as m


if __name__ == "__main__":
    for show in m.RadioShow.objects.filter(completeness='Gold'):
        data = defaultdict(list)
        broadcast = show.performances.all()[0]
        associated_agents = m.AssociatedAgent.objects.filter(recording=broadcast, roles__role="Performer")
        for aa in associated_agents:
            roles = aa.roles.all().values_list('role')
            data[aa.agent.name].append(aa)
        for name, name_list in data.items():
            if len(name_list) > 1:
                for dupe_aa in name_list[1:]:
                    dupe_aa.delete()

