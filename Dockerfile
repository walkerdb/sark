# Start with a Python image.
FROM python:3-alpine3.9

# Some stuff that everyone has been copy-pasting
# since the dawn of time.
ENV PYTHONUNBUFFERED 1

# Install some necessary things.
RUN apk update
RUN apk -U add dpkg-dev netcat-openbsd swig postgresql-dev \
    libxml2-dev libxslt-dev libffi-dev gcc musl-dev libgcc libressl-dev curl \
    jpeg-dev zlib-dev freetype-dev lcms2-dev openjpeg-dev tiff-dev tk-dev tcl-dev bash git


# Copy all our files into the image.
RUN mkdir -p /sark
WORKDIR /sark
COPY . /sark/

# Install our requirements.
RUN pip install -U pip
RUN pip install -Ur requirements.txt
