#!/usr/bin/env bash
#sleep 15
export DJANGO_SETTINGS_MODULE=sark_django.settings.docker
pip install -Ue git+https://github.com/django-haystack/django-haystack.git#egg=haystack
#./manage.py flush --noinput
./manage.py makemigrations
./manage.py migrate
#./manage.py preload_data
#./manage.py enable_access_to_recordings "./sark_django/sark/management/commands/batch_definitions/INITIAL_31_BROADCASTS.csv"
./manage.py enable_access_to_all_recordings
./manage.py build_solr_schema -c /solr/sark/conf/
./manage.py update_index --remove

./manage.py shell -c \
"from django.contrib.auth.models import User; User.objects.filter(username='${ADMIN_USER}').exists() or User.objects.create_superuser('${ADMIN_USER}', '${ADMIN_EMAIL}', '${ADMIN_PASS}')"

./manage.py shell -c \
"from django.contrib.auth.models import User; User.objects.filter(username='${ADMIN_STUDENT_USER}').exists() or User.objects.create_superuser('${ADMIN_STUDENT_USER}', '${ADMIN_STUDENT_EMAIL}', '${ADMIN_STUDENT_PASS}')"

gunicorn --workers 3 --bind "0.0.0.0:8000" sark_django.wsgi
