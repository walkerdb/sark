#!/usr/bin/env bash

# crontab: 30 6 * * * /webApps/sark/backup_db.sh
# you can see backups at /var/lib/docker/volumes/sark_data/_data/backups
# to load from backup just exec 'psql postgres --username=postgres < backup_DATE.bak'

docker exec -w /var/lib/postgresql/data/backups sark_db_1 bash -c 'pg_dump postgres --username=postgres > backup_$(date +%Y-%m-%d).bak'