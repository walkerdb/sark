#!/usr/bin/env bash

deploy_user="${DEPLOY_USER}"
server="$1"

deploy_server="${deploy_user}@${server}"
deploy_dir="/webApps/sark"
media_dir="/webApps/sark_media"

ssh "${deploy_server}" docker-compose -f "${deploy_dir}/docker-compose.yml" stop
ssh "${deploy_server}" rm -rf "${deploy_dir}/"
ssh "${deploy_server}" mkdir -p "${deploy_dir}/"
scp -rp . "${deploy_server}:${deploy_dir}"
ssh "${deploy_server}" ln -s "${media_dir}/audio/*" "${deploy_dir}/sark_django/static/sarkfiles/audio/"
ssh "${deploy_server}" ln -s "${media_dir}/img/*" "${deploy_dir}/sark_django/static/sarkfiles/img/"
ssh "${deploy_server}" cp "/home/${deploy_user}/secrets/.env" "${deploy_dir}/"
ssh "${deploy_server}" docker-compose -f "${deploy_dir}/docker-compose.yml" build
ssh "${deploy_server}" docker-compose -f "${deploy_dir}/docker-compose.yml" up -d

# check in on status of prod box
