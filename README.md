# Music Time in Africa Archive

## How it works
The MTiA Archive has 4 major parts:
1. a django application that generates all web pages and generally orchestrates the site. Most of this repo is code for this django app.
1. an nginx server that routes web traffic either to the django app or to the static media folder 
   (for images and audio).
1. a postgres db to store all the data. It gets backed up daily.
1. a solr instance for fuzzy text searching and result ranking. This runs all filtering, categorization, and the search function.

They're all run in production through `docker-compose`. You can run 
an instance yourself by building the django image with `docker-compose build web` then `docker-compose up` to start everything up.
The repo is expected to live at `/webApps/sark`, and static assets are expected to live at `webApps/sark_media`.

### Audio and Images
To avoid commiting large binaries, all audio and images are expected to live a separate directory somewhere, with all individual files
symlinked into `sark_django/static/sarkfiles`. I usually run excerpts from the [sync_files_and_make_symlinks.sh](sark_django/migration_scripts/sync_files_and_make_symlinks.sh) script
to do this.

nginx is set up to treat requests to `/static` as requests for static assets, which it routes
into `/webApps/sark/sark_django/static/sarkfiles`. Resources in that folder are symlinks to files
living in `/webApps/sark_media`.

## Adding new broadcasts
1. commit the new xml metadata file to the repo, as something like `metadata_[barcode].xml`
1. create web-friendly access mp3s. Name them `[barcode].mp3`
1. create web-friendly access jpg files for any related images. Name them `[barcode]-[name]-[number-with-leading-0].jpg`.
   eg `39015090286991-script-01.jpg`
1. copy the new audio files to `/webApps/sark_media/audio` on the remote server
1. copy the new image files to `/webApps/sark_media/img` on the remote server
1. create symlinks to these new media files to `/webApps/sark/sark_django/static/sarkfiles/audio` and `img`.
   See [sync_files_and_make_symlinks.sh](sark_django/migration_scripts/sync_files_and_make_symlinks.sh) for an example.
1. Add a new function to [import_xml_to_db.py](sark_django/migration_scripts/import_xml_to_db.py) that specifically just reads
   the new xml files, then create a new command that calls the function.
1. Run your command in the prod docker container, then run the [enable_access_to_all_recordings](sark_django/sark/management/commands/enable_access_to_all_recordings.py) command.
   ```bash
   docker exec -it <container_id> /bin/bash
   export DJANGO_SETTINGS_MODULE=sark_django.settings.docker
   ./manage.py my_command
   ./manage.py enable_access_to_all_recordings

   ```
